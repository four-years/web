<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DaHa
  Date: 2021/3/22
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>更新书籍</title>
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
</head>

<body>
<jsp:useBean id="user" class="cn.edu.niit.javabean.User"
             scope="session"/>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">修改图书</div>
                <div class="layui-card-body" pad15>
                    <div class="layui-row">

                        <div class="layui-col-md10">
                            <form action="/book/updateBook1"
                                  method="post">
                                <div class="layui-form" lay-filter="">
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">书名</label>
                                        <div class="layui-input-inline" >
                                            <input type="text"
                                                   name="bookname"
                                                   value="${book.name}"
                                                   placeholder="请设置书名"
                                                   class="layui-input"/>
                                        </div>

                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label">作者</label>
                                        <div class="layui-input-inline">
                                            <input type="text"
                                                   name="author"
                                                   value="${book.author}"
                                                   lay-verify="author"
                                                   autocomplete="off"
                                                   placeholder="请输入作者"
                                                   class="layui-input">
                                        </div>
                                    </div>


                                    <div class="layui-form-item">
                                        <label class="layui-form-label">分类</label>
                                        <div class="layui-input-inline">
                                            <input type="text"
                                                   name="sort"
                                                   value="${book.sort}"
                                                   lay-verify="sort"
                                                   autocomplete="off"
                                                   class="layui-input" placeholder="请设置分类">
                                        </div>
                                    </div>

                                    <div class="layui-form-item layui-form-text">
                                        <label class="layui-form-label">描述</label>
                                        <div class="layui-input-block">
										<textarea name="description"
                                                  placeholder="请输入内容"
                                                  class="layui-textarea">${book.description}
										</textarea>
                                        </div>
                                    </div>


                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <input type="submit"
                                                   class="layui-btn"
                                                   value="确认修改"
                                                   lay-filter="setmyinfo">
                                            </input>
                                            <button type="reset"
                                                    class="layui-btn layui-btn-primary">
                                                重新填写
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/layui/layui.js"></script>
<script>
    layui.use(['form', 'upload'], function () {
        var form = layui.form;
        var upload = layui.upload;
        var $ = layui.$;


        // //执行实例
        // var uploadInst = upload.render({
        //     elem: '#LAY_avatarUpload' //绑定元素
        //     , url: '/upload/' //上传接口
        //     , done: function (res) {
        //         //上传完毕回调
        //     }
        //     , error: function () {
        //         //请求异常回调
        //     }
        // });


        //各种基于事件的操作，下面会有进一步介绍
    });
</script>
</body>

</html>
