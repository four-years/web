<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2021/5/3
  Time: 19:00
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"
          media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->


</head>
<body>

<div>
    <h2>${requestScope.book.name}</h2>
    <h2>${requestScope.book.description}</h2>

</div>
<div>
    <button class="layui-btn layui-btn-xs borrow"
            id="collection" index="${book.id}">
        ${sessionScope.books[book.id].collection?"收藏":"已收藏"}

    </button>
    <button class="layui-btn layui-btn-xs borrow"
            id="rent" index="${book.id}">
        ${sessionScope.books[book.id].rent?"借阅":"已借阅"}
    </button>
</div>
<div id="comment_list" style="font-size: 20px;">
    <c:forEach var="comment" items="${sessionScope.comments}">
        <div class="layui-col-md2"
             style="display: flex;">
            <div>
                <img class="layui-header" style="border-radius:50%;width:70px;height:70px;"
                      src="${comment.header}"/></div>
            <div style="margin: 5px">
                <div class="layui-form-item">${comment.reader}</div>
                <div class="layui-form-item">${comment.createTime}</div>
            </div>


        </div>


        <div class="layui-form-item">${comment.comment}</div>
    </c:forEach>
</div>

</body>

<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['laypage', 'layer','upload'], function () {
        var $ = layui.$;
        var upload = layui.upload;

        $("#head", parent.document).attr('src',
            '${sessionScope.comment.header}');
        $("#reader", parent.document).html(
            '${sessionScope.comment.header}');

        //执行实例
        var uploadInst = upload.render({
            elem: '#LAY_avatarUpload' //绑定元素
            , url: '/upload/' //上传接口
            , done: function (res) {
                //上传完毕回调
            }
            , error: function () {
                //请求异常回调
            }
        });


        $(document).on('click', '#rent', function () {
            //可以获取第一列的内容，也就是name的值
            var name = $(this).parents("tr").find("td").eq(0).text();
            var elem = $(this);
            var bookid = $(this).attr("index");
            $.ajax({
                type: 'POST',
                url: "/book/rent",
                data: JSON.stringify({
                    user: ${sessionScope.id}+"",
                    book: bookid
                }),
                contentType: "application/json;charset=utf-8",
                success: function (data) {

                    //count从Servlet中得到
                    // count = data;

                    if (data == '借阅成功') {
                        layer.msg(data)
                        elem.text("已借阅");
                        //$('#rent').text("已借阅")
                        //$('#rent').load(location.href + " #rent");
                    } else if (data == "取消借阅") {
                        layer.msg(data)
                        elem.text("借阅");
                    }

                }
            });
            //location.href = location.href;

        })

        $(document).on('click', '#collection', function () {
            //可以获取第一列的内容，也就是name的值
            //$(this)指的是当前点击的这个按钮，obj.parents("选择器");//找符合选择器的祖先节点，eq(index) 下标等于index 从0开始
            //var name = $(this).parents("tr").find("td").eq(1).text();

            var elem = $(this);
            var bookid = $(this).attr("index");
            $.ajax({
                type: 'POST',
                url: "/book/collection",
                data: JSON.stringify({
                    user: ${sessionScope.id}+"",
                    book: bookid
                }),
                contentType: "application/json;charset=utf-8",
                success: function (data) {

                    //count从Servlet中得到
                    // count = data;

                    if (data == '收藏成功') {
                        layer.msg(data)
                        elem.text("已收藏");
                        //$('#rent').text("已借阅")
                        //$('#rent').load(location.href + " #rent");
                    } else if (data == "取消收藏") {
                        layer.msg(data)
                        elem.text("收藏");
                    }

                }
            });
            //location.href = location.href;

        })



        $(document).ready(function () {
            var bookid = ${requestScope.book.id};
            console.log(bookid);
            $.ajax({
                type: 'GET',
                url: "/book/comment?id="+bookid,
                async: false, //开启同步请求，为了保证先得到count再渲染表格
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    $('#comment_list').load(location.href + " #comment_list");
                }
            });
        });
    });

</script>
</html>
