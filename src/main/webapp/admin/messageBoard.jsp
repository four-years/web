<%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2021/5/6
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>留言板</title>
</head>
<body>
<div class="content">
    <div>
        <form action="/book/addMessage" method="post" class="message">
            <h3>🍇message：<input value="${sessionScope.user.username}" name="username"/></h3>
                <textarea id="message" name="message" placeholder="Your Message to Us" cols="100" rows="10"></textarea>
                <div class="s">
                    <input type="submit" value="发表">

                    <input type="reset" value="取消">
                </div>


        </form>
    </div>
    <div class="line"></div>
    <div class="con">
        <h4>留言列表</h4>

        <c:forEach var="messageBorder" items="${sessionScope.messageBorderList}">
            <div class="conmain">
                <div class="conmain1">
                    <div><img src="${messageBorder.header}" class="image"></div>
                    <div class="right">
                        <span class="mname">${messageBorder.reader}</span>
                        <span class="comment">${messageBorder.content}</span>
                    </div>
                </div>

                <div class="right1">
                    <a href="#">删除🌂</a>
                </div>
            </div>
            <div  class="time">
                <span>${messageBorder.createTime}</span>
                <div class="right1">
                    <div><a href="#">回复</a></div>
                    <div style="padding-left: 10px"><a href="#">送礼</a></div>
                </div>
            </div>
            <div class="line"></div>
        </c:forEach>
    </div>
</div>

</body>
<style>
    body{
        background: url("/img/back.jpg");
        background-repeat: no-repeat;
        background-size:100% 100%;
        background-attachment: fixed;
    }
    .content{
        width: 90%;
        height: 100%;
        background-color: white;
        opacity: 0.7;
        margin: 0 auto;
        overflow: scroll;
    }
    .message{
        width: 50%;
        margin: 0 auto;
    }
    textarea{
        outline:none;
        resize:none;
        background:transparent;
        border-color:#f0f0f0;
        overflow-y:hidden;
        overflow-x:hidden;
    }
    .s{
        margin-top: 10px;
    }
    .image{
        width: 70px;
        height: 70px;
        border-radius: 10px;
    }
    .line{
        border-top: 1px solid #f0f0f0;
        margin-top: 20px;
    }
    .con{
        width: 90%;
        margin: 0 auto;
    }
    .conmain{

        display: flex;justify-content: space-between; box-pack: justify;
    }
    .conmain1{
        display: flex;
        flex-direction: row;
        margin-top: 20px;
    }
    .right{
        display: flex;
        flex-direction: column;
        padding: 10px;
    }
    .comment{
        font-size: 15px;
        margin-top: 20px;
    }
    .mname{
        font-size: 17px;
        font-family: "Kristen ITC";
        font-weight: bold;
        color: #2D93CA;
    }
    .time{
        margin-top: 40px;
        display: flex;
        flex-direction: row;
    }
    .right1{
        color: #2D93CA;
        display: flex;
        flex-direction: row;
        margin-left: 20px;
        padding-left: 10px;
    }
</style>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['laypage', 'layer','upload'], function () {
        var $ = layui.$;
        var upload = layui.upload;
        $("#reader", parent.document).html(
            '${sessionScope.user.reader}');

        //执行实例
        var uploadInst = upload.render({
            elem: '#LAY_avatarUpload' //绑定元素
            , url: '/upload/' //上传接口
            , done: function (res) {
                //上传完毕回调
            }
            , error: function () {
                //请求异常回调
            }
        });


        $(document).ready(function () {
            $.ajax({
                type: 'POST',
                url: "/book/messageBorder",
                data: JSON.stringify({
                    user:${sessionScope.id}+""
                }),
                async: false, //开启同步请求，为了保证先得到count再渲染表格
                contentType: "application/json;charset=utf-8",
                success: function (data) {

                }
            });
        });
    });

</script>

</html>
