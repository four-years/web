<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: DaHa
  Date: 2021/3/22
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<jsp:useBean id="user" class="cn.edu.niit.javabean.User"
             scope="session"/>
<head>
    <meta charset="utf-8">
    <title>增加用户</title>
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
</head>

<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">更新用户</div>
                <div class="layui-card-body" pad15>
                    <div class="layui-row">

                        <div class="layui-col-md10">
                            <form action="/book/updateUser1"
                                  method="post">
                                <div class="layui-form" lay-filter="">
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">id</label>
                                        <div class="layui-input-inline">
                                            <input type="text"
                                                   name="id"
                                                   value="${user.id}"
                                                   readonly
                                                   placeholder="请设置账户"
                                                   class="layui-input"/>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">账户</label>
                                        <div class="layui-input-inline" >
                                            <input type="text"
                                                   name="username"
                                                   value="${user.username}"
                                                   placeholder="请设置账户"
                                                   class="layui-input"/>
                                        </div>

                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">密码</label>
                                        <div class="layui-input-inline">
                                            <input type="password"
                                                   name="password"
                                                   value="${user.password}"
                                                   placeholder="请设置密码"
                                                   class="layui-input"/>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">昵称</label>
                                        <div class="layui-input-inline">
                                            <input type="text"
                                                   name="nickname"
                                                   value="${user.reader}"
                                                   lay-verify="nickname"
                                                   autocomplete="off"
                                                   placeholder="请输入昵称"
                                                   class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">性别</label>
                                        <div class="layui-input-block">
                                            <input type="radio"
                                                   name="sex"
                                                   value="男"
                                                   title="男"

                                                    <%=user.isSex() ? "checked" : ""%>/>
                                            <input type="radio"
                                                   name="sex"
                                                   value="女"
                                                   title="女" <%=user.isSex() ? "" : "checked"%>/>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label">手机</label>
                                        <div class="layui-input-inline">
                                            <input type="text"
                                                   name="cellphone"
                                                   value="${user.cellphone}"
                                                   lay-verify="cellphone"
                                                   autocomplete="off"
                                                   class="layui-input" placeholder="请设置手机">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">邮箱</label>
                                        <div class="layui-input-inline">
                                            <input type="text"
                                                   name="email"
                                                   value="${user.email}"
                                                   lay-verify="email"
                                                   autocomplete="off"
                                                   class="layui-input" placeholder="请设置邮箱">
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <input type="submit"
                                                   class="layui-btn"
                                                   value="确认修改"
                                                   lay-filter="setmyinfo">
                                            </input>
                                            <button type="reset"
                                                    class="layui-btn layui-btn-primary">
                                                重新填写
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/layui/layui.js"></script>
<script>
    layui.use(['form', 'upload'], function () {
        var form = layui.form;
        var upload = layui.upload;
        var $ = layui.$;


        //执行实例
        var uploadInst = upload.render({
            elem: '#LAY_avatarUpload' //绑定元素
            , url: '/upload/' //上传接口
            , done: function (res) {
                //上传完毕回调
            }
            , error: function () {
                //请求异常回调
            }
        });


        //各种基于事件的操作，下面会有进一步介绍
    });
</script>
</body>

</html>
