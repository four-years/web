<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: asus
  Date: 2021/3/15
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%
        //获取传递参数用户名和密码
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        //加载驱动
        Class.forName("com.mysql.cj.jdbc.Driver");
        //创建连接
        try(Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/library?serverTimezone=UTC&characterEncoding=UTF-8","root","Xiaoming250")){
            //预编译sql语句，自动关闭响应的内容
            String sql="select * from borrow_card where username=?";
            try(PreparedStatement statement=connection.prepareStatement(sql)) {
                statement.setString(1, username);
                //执行查询
                ResultSet resultSet = statement.executeQuery();
                //遍历
                while (resultSet.next()) {
                    if (password.equals(resultSet.getString("password"))) {
                        //执行跳转
                        response.sendRedirect("./main.jsp");
                    } else {
                        //返回首页
                        response.sendRedirect("./index.jsp");
                    }
//                    //无此用户
//                    response.sendRedirect("./index.jsp");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    %>
</body>
</html>
