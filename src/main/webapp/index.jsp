<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>
    </title>
    <style>
        body {
            background: url("/img/4.gif");
            background-repeat: no-repeat;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            min-width: 1000px;
            z-index: -10;
            zoom: 1;
            background-size: cover;
            background-position: center 0;
        }

        .content {
            position: absolute;
            width: 600px;
            height: 350px;
            background: pink;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            border-radius: 15px;
            padding-top: 10px;
            opacity: 0.7;
        }

        .login {
            position: absolute;
            margin: auto;
            width: 450px;
            height: 200px;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            font-size: larger;
        }

        .title {
            text-align: center;
            font-size: xx-large;
            color: white;
            padding: 1px;
        }

        .layui-input-block {
            width: 300px;
        }

        .layui-form-item {
            padding: 10px;
        }

        .btn {
            margin-top: 20px;
            height: 30px;
            width: 100px;
            background-color: pink;
            font-size: larger;
            color: white;
        }

        .btn-btn {
            display: flex;
            justify-content: space-between;
            width: 80%;
            margin: 0 auto;
        }


    </style>
    <link rel="stylesheet" href="layui/css/layui.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/layui/css/layui.css"/>
    <script type="text/javascript" src="<%=request.getContextPath()%>/layui/layui.js"></script>
</head>

<body>
<%@ include file="window.jsp" %>

<div class="content">
    <div class="title">图书馆管理系统登录界面</div>
    <div class="login">

        <form class="layui-form" action="/login" method="post">
            <div class="layui-form-item">
                <label class="layui-form-label">用户名：</label>
                <div class="layui-input-block">
                    <input type="text" name="username" required lay-verify="required" placeholder="请输入用户"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">密&nbsp&nbsp&nbsp码：</label>
                <div class="layui-input-block">
                    <input type="password" name="password" required lay-verify="required" placeholder="请输入密码"
                           autocomplete="off" class="layui-input">
                </div>

            </div>
            <div class="layui-inline">
<%--                <label class="layui-form-label">管理员登录</label>--%>
<%--                <div class="layui-input-block">--%>
<%--                    <input type="checkbox" lay-skin="switch"--%>
<%--                           lay-filter="admin">--%>
<%--                </div>--%>
                <div class="layui-input-block">
                    <input type="radio" value="0" title="管理员" name="role">
                    <input type="radio" value="1" title="用户" name="role" checked>
                </div>
            </div>

            <div class="btn-btn">
                <button class="btn" lay-submit lay-filter="formDemo">登录</button>
               <button class="btn"><a href="register.jsp" style="color: #fff">注册</a></button>
            </div>

        </form>
    </div>


</div>

</body>

<script>

    layui.use('form',function (){
        var form=layui.form;
        var $=layui.$;
        $("#admin").click(function (){
            var result=$("admin").is(':checked');
        })
    });
</script>
</html>
