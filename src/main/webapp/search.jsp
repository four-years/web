<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="./layui/css/layui.css"
          media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
    <style>
        .wrap-div {
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 3;
            overflow: hidden;
            float: left;
            width: 100%;
            word-break: break-all;
            text-overflow: ellipsis;
        }
    </style>
</head>
<body>

<div class="layui-form" id="content">
    <table class="layui-table" style="table-layout:fixed">
        <colgroup>
            <col width="150">
            <col width="150">
            <col width="200">
            <col>
            <col width="180">
        </colgroup>
        <thead>
        <tr>
            <th>书名</th>
            <th>作者</th>
            <th>分类</th>
            <th>描述</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="book" items="${sessionScope.bookList}"
                   varStatus="status">
            <tr>
                <td>${book.name}</td>
                <td>${book.author}</td>
                <td>${book.sort}</td>
                <td class="wrap-td">
                    <div class="wrap-div">${book.description}</div>
                </td>
                <td>
                        <%--                    这边通过a标签的href跳转到详情界面，因为href的请求是get请求（记住这种方式是get请求），所以直接把id参数放在了url中--->?id=${book.id}--%>
                    <button
                            class="layui-btn layui-btn-primary layui-btn-xs detail"
                            id="info"><a href="/book/lookBook?id=${book.id}">查看</a>
                    </button>
                    <button class="layui-btn layui-btn-xs borrow"
                            id="collection" index="${book.id}">
                            ${book.collection?"已收藏":"收藏"}

                    </button>
                    <button class="layui-btn layui-btn-xs borrow"
                            id="rent" index="${book.id}">
                            ${book.rent?"已借阅":"借阅"}
                    </button>

                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div id="page" style="display: flex;justify-content: center;"></div>

<script src="./layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述 JS 路径需要改成你本地的 -->
<script>
    var loc = location;
    layui.use(['laypage', 'layer'], function () {
            var laypage = layui.laypage, layer = layui.layer;
            var $ = layui.$;
            var count = 0, page = 1, limit = 5;


            $(document).ready(function () {
                //进入页面先加载数据
                getContent(1, limit);
                //得到数量count后，渲染表格
                laypage.render({
                    elem: 'page',
                    count: count,
                    curr: page,
                    limits: [5, 10, 15, 20],
                    limit: limit,
                    layout: ['count', 'prev', 'page', 'next', 'limit'],
                    jump: function (obj, first) {
                        if (!first) {
                            getContent(obj.curr, obj.limit);
                            //更新当前页码和当前每页显示条数
                            page = obj.curr;
                            limit = obj.limit;
                        }
                    }
                });
            });

            $(document).on('click', '#rent', function () {
                //可以获取第一列的内容，也就是name的值
                var name = $(this).parents("tr").find("td").eq(0).text();
                var elem = $(this);
                var bookid = $(this).attr("index");
                $.ajax({
                    type: 'POST',
                    url: "/book/rent",
                    data: JSON.stringify({
                        user: ${sessionScope.id}+"",
                        book: bookid
                    }),
                    contentType: "application/json;charset=utf-8",
                    success: function (data) {

                        //count从Servlet中得到
                        // count = data;

                        if (data == '借阅成功') {
                            layer.msg(data)
                            elem.text("已借阅");
                            //$('#rent').text("已借阅")
                            //$('#rent').load(location.href + " #rent");
                        } else if (data == "取消借阅") {
                            layer.msg(data)
                            elem.text("借阅");
                        }

                    }
                });
                //location.href = location.href;

            })

            $(document).on('click', '#collection', function () {
                //可以获取第一列的内容，也就是name的值
                //$(this)指的是当前点击的这个按钮，obj.parents("选择器");//找符合选择器的祖先节点，eq(index) 下标等于index 从0开始
                //var name = $(this).parents("tr").find("td").eq(1).text();

                var elem = $(this);
                var bookid = $(this).attr("index");
                $.ajax({
                    type: 'POST',
                    url: "/book/collection",
                    data: JSON.stringify({
                        user: ${sessionScope.id}+"",
                        book: bookid
                    }),
                    contentType: "application/json;charset=utf-8",
                    success: function (data) {

                        //count从Servlet中得到
                        // count = data;

                        if (data == '收藏成功') {
                            layer.msg(data)
                            elem.text("已收藏");
                            //$('#rent').text("已借阅")
                            //$('#rent').load(location.href + " #rent");
                        } else if (data == "取消收藏") {
                            layer.msg(data)
                            elem.text("收藏");
                        }

                    }
                });
                //location.href = location.href;

            })


            function getContent() {
                $.ajax({
                    type: 'POST',
                    url: "/book/searchk",
                    async: false, //开启同步请求，为了保证先得到count再渲染表格
                    data: JSON.stringify({
                        pageNum: page,
                        pageSize: size
                    }),
                    contentType: "application/json;charset=utf-8",
                    success: function (data) {
                        $('#content').load(location.href + " #content");
                        //count从Servlet中得到
                        count = data;
                    }
                });
            }
        }
    );
</script>

</body>
</html>
