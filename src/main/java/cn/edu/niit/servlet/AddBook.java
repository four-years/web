package cn.edu.niit.servlet;

import cn.edu.niit.javabean.Book;
import cn.edu.niit.service.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/28 18:13
 * @Version 1.0
 */
@WebServlet(name = "AddBook",urlPatterns = "/book/addBook")
public class AddBook extends HttpServlet {
    private BookService bookService=new BookService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Book book=new Book();
        book.setName(req.getParameter("bookname"));
        book.setAuthor(req.getParameter("author"));
        book.setSortId(Integer.valueOf(req.getParameter("sort")));
        book.setDescription(req.getParameter("description"));
        bookService.insertBook(book);
        resp.sendRedirect("/admin/borrowHistory.jsp");
    }
}
