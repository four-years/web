package cn.edu.niit.servlet;

import cn.edu.niit.javabean.dto.UserDto;
import cn.edu.niit.service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(name = "RegisterServlet", urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {
    private LoginService loginService = new LoginService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置字符编码集
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");

        //获取注册界面输入得内容
        String name = req.getParameter("username");

        String password = req.getParameter("pwd");
        String pwdComfirm = req.getParameter("pwd_confirm");
        if (password.equals(pwdComfirm)) {

            UserDto userDto = new UserDto();
            userDto.setAccount(name);
            userDto.setPassword(password);

            int lines = loginService.addUser(userDto);
            if (lines>0){
                resp.sendRedirect("/index.jsp?message="+ URLEncoder.encode("注册成功", "utf-8"));
            }else{
                req.getRequestDispatcher("/register.jsp?message=注册失败").forward(req, resp);
            }
        }else{
            req.getRequestDispatcher("/register.jsp?message=密码两次输入不一致").forward(req,resp);
        }

    }
}
