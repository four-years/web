package cn.edu.niit.servlet;

import cn.edu.niit.service.AdminService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet(name = "AdminLoginServlet",urlPatterns = "/admin_login")
public class AdminLoginServlet extends HttpServlet {
    private AdminService adminService=new AdminService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取输入框填入的内容
        String username=req.getParameter("username");

        String password=req.getParameter("password");

        String result= adminService.login(username,password,req.getSession());

        if("登录成功".equals(result)){
            //重定向
            resp.sendRedirect("/admin/main.jsp");

        }else{
            //转发
            req.getRequestDispatcher("/index.jsp?message=" + result).forward(req,resp);
        }
    }
}
