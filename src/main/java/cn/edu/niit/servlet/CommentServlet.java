package cn.edu.niit.servlet;

import cn.edu.niit.javabean.Comment;
import cn.edu.niit.service.CommentService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/7 11:18
 * @Version 1.0
 */
@WebServlet(name = "CommentServlet", urlPatterns = "/book/comment")
public class CommentServlet extends HttpServlet {
    private CommentService commentService=new CommentService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

       int id=Integer.valueOf(req.getParameter("id"));
//        String paramJson = IOUtils.toString(
//                req.getInputStream(), "UTF-8");
//        HashMap<String, Object> parseObject =
//                JSON.parseObject(paramJson,
//                        HashMap.class);
//        //2.从点击界面中获取bookId的值
//        int id = (int) parseObject.get("id");

        List<Comment> commentList=new ArrayList<>();
        commentList = commentService.commentList(id);
        req.getSession().setAttribute("comments",commentList);

        req.getRequestDispatcher("/lookBook.jsp").forward(req,resp);

    }
}
