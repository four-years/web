package cn.edu.niit.servlet;
import cn.edu.niit.javabean.Collection;
import cn.edu.niit.service.CollectionService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/18 16:29
 * @Version 1.0
 */
@WebServlet(name = "FavriteServlet", urlPatterns = "/book/favorite")
public class FavriteServlet extends HttpServlet {
    private CollectionService collectionService=new CollectionService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取参数
        String paramJson = IOUtils.toString(
                req.getInputStream(), "UTF-8");
        HashMap<String, Object> parseObject =
                JSON.parseObject(paramJson,
                        HashMap.class);
        String param = (String) parseObject.get("search");

        int pageNum = (int) parseObject.get("pageNum");
        int pageSize = (int) parseObject.get("pageSize");
        String username = (String) parseObject.get("user");
        List<Collection> collections = new ArrayList<>();
        int count = 0;
        //2.
        if (param != null) {
            //带参数查询
        } else {
            //无参查询
            collections= collectionService.collectionList(username,pageNum,
                    pageSize);


        }

        count = collectionService.countNum();

        //3. 将结果放入session
        req.getSession().setAttribute("collections", collections);

        //将count直接作为ajax请求的结果返回
        resp.getWriter().print(count);
    }
}
