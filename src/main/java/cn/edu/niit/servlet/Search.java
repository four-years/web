package cn.edu.niit.servlet;

import cn.edu.niit.javabean.Book;
import cn.edu.niit.service.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/30 22:55
 * @Version 1.0
 */
@WebServlet(name = "Search",urlPatterns = "/book/searchk")
public class Search extends HttpServlet {
    private BookService bookService = new BookService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //根据图书名进行模糊查询
        String keyword = req.getParameter("keyword");
        List<Book> bookList = new ArrayList<Book>();
        String msg = "搜索失败，请重新搜索！";
        if (keyword !=null){
            bookList = bookService.search(keyword);
            System.out.println(bookList);
            resp.sendRedirect("/search.jsp");
        }else {
            resp.getWriter().println("<script>alert('"+msg+"')</script>");
        }
        req.getSession().setAttribute("bookList",bookList);
    }

}
