package cn.edu.niit.servlet;

import cn.edu.niit.javabean.User;
import cn.edu.niit.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/30 0:13
 * @Version 1.0
 */
@WebServlet(name = "UpdateUser",urlPatterns = "/book/updateUser")
public class UpdateUser extends HttpServlet {
    private UserService userService=new UserService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id=Integer.valueOf(req.getParameter("id"));

        User user=userService.selectById(id);
        System.out.println(user.isSex());

//        resp.getWriter().print("<script>location.reload()" +
//                "</script>");
        //转向会原页面，刷新页面
        req.setAttribute("fresh", true);
        req.setAttribute("user",user);
        req.getRequestDispatcher("/admin/updateUser.jsp").forward(req, resp);
    }
}
