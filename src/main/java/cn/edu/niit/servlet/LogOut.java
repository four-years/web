package cn.edu.niit.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/28 9:49
 * @Version 1.0
 */
@WebServlet(name = "LogOut",urlPatterns = "/book/logout")
public class LogOut extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter pw=resp.getWriter();
        HttpSession session=req.getSession(true);
        session.invalidate();
        this.getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(req,resp);
        pw.flush();
        pw.close();
    }
}
