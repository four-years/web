package cn.edu.niit.servlet;

import cn.edu.niit.javabean.MessageBorder;
import cn.edu.niit.service.MessageBorderService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/27 15:18
 * @Version 1.0
 */
@WebServlet(name = "AddMessageBorder", urlPatterns = "/book/addMessage")
public class AddMessageBorder extends HttpServlet {
    private MessageBorderService messageBorderService=new MessageBorderService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String messages=req.getParameter("message");

        String username = req.getParameter("username");
        int m=messageBorderService.insertMessageBorder(username,messages);
        if (m>0){
            resp.sendRedirect("/messageBoard.jsp?message="+ URLEncoder.encode("添加成功", "utf-8"));
        }else{
            req.getRequestDispatcher("/messageBoard.jsp?message=注册失败").forward(req, resp);
        }
    }
}
