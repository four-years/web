package cn.edu.niit.servlet;

import cn.edu.niit.service.BookService;

import org.apache.commons.io.IOUtils;
import com.alibaba.fastjson.JSON;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/4/29 16:50
 * @Version 1.0
 */
@WebServlet(name = "RentBookServlet", urlPatterns = "/book/rent")
public class RentBookServlet extends HttpServlet {
    private BookService bookService = new BookService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1. 取参（req当前的页码, 每页的数量, 搜索）
        String paramJson = IOUtils.toString(
                req.getInputStream(), "UTF-8");
        HashMap<String, Object> parseObject =
                JSON.parseObject(paramJson,
                        HashMap.class);
        String username = (String) parseObject.get("user");
        String bookId = (String) parseObject.get("book");

        String message = bookService.rentBook(username, bookId);
        resp.getWriter().print(message);
    }
}
