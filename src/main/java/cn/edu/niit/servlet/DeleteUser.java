package cn.edu.niit.servlet;

import cn.edu.niit.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/29 22:01
 * @Version 1.0
 */
@WebServlet(name = "DeleteUser",urlPatterns = "/book/deleteUser")
public class DeleteUser extends HttpServlet {
    private UserService userService=new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id=Integer.valueOf(req.getParameter("id"));
        int i=userService.deleteUser(id);
        resp.sendRedirect("/admin/userList.jsp");
    }
}
