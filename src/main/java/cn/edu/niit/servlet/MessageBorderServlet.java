package cn.edu.niit.servlet;

import cn.edu.niit.javabean.MessageBorder;
import cn.edu.niit.service.MessageBorderService;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/26 15:05
 * @Version 1.0
 */
@WebServlet(name = "MessageBorderServlet", urlPatterns = "/book/messageBorder")
public class MessageBorderServlet extends HttpServlet {
    private MessageBorderService messageBorderService=new MessageBorderService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<MessageBorder> messageBorderList= messageBorderService.messageBorder();
        req.setAttribute("fresh", true);
        req.getSession().setAttribute("messageBorderList",messageBorderList);

    }
}
