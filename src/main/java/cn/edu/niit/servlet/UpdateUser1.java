package cn.edu.niit.servlet;

import cn.edu.niit.javabean.User;
import cn.edu.niit.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/30 21:27
 * @Version 1.0
 */
@WebServlet(name = "UpdateUser1",urlPatterns = "/book/updateUser1")
public class UpdateUser1 extends HttpServlet {
    private UserService userService=new UserService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接受参数

        User user=new User();
        user.setId(Integer.valueOf(req.getParameter("id")));
        user.setUsername(req.getParameter("username"));
        user.setEmail(req.getParameter("email"));
        user.setPassword(req.getParameter("password"));
        user.setCellphone(req.getParameter("cellphone"));
        user.setReader(req.getParameter("nickname"));
        String sex=req.getParameter("sex");
        boolean value=false;
        if ("男".equals(sex)) {
            value= true;
        } else {
            value = false;
        }
        user.setSex(value);



        userService.updateUser(user);
        resp.sendRedirect("/admin/userList.jsp");
    }
}
