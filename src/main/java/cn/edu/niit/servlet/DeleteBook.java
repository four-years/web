package cn.edu.niit.servlet;

import cn.edu.niit.javabean.Book;
import cn.edu.niit.service.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/29 21:22
 * @Version 1.0
 */
@WebServlet(name = "DeleteBook",urlPatterns = "/book/deleteBook")
public class DeleteBook extends HttpServlet {
    private BookService bookService=new BookService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));

        //3.获取bookId的列表，然后通过book的id进行查询，查出这本书,将查出来的这本书的信息封装到了book对象中
        int i = bookService.deleteBook(id);
        resp.sendRedirect("/admin/borrowHistory.jsp");


    }
}
