package cn.edu.niit.servlet;

import cn.edu.niit.service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "LoginServlet",urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private LoginService loginService=new LoginService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //获取输入框填入的内容
        String username=req.getParameter("username");

        String password=req.getParameter("password");

        String result=loginService.login(username,password,req.getSession());
        if("登录成功".equals(result)){
            //重定向
            resp.sendRedirect("/main.jsp");

        }else{
            //转发
            req.getRequestDispatcher("/index.jsp?message=" + result).forward(req,resp);
        }
    }
}
