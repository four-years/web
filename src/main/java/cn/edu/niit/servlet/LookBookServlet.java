package cn.edu.niit.servlet;

import cn.edu.niit.javabean.Book;
import cn.edu.niit.service.BookService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.HashMap;


/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/3 22:03
 * @Version 1.0
 */
@WebServlet(name = "LookBookServlet", urlPatterns = "/book/lookBook")
public class LookBookServlet extends HttpServlet {
    private BookService bookService=new BookService();

//    然后请求就到了这边，注意重写的这个方法是doGet方法，因为刚才说了，href是get请求
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //怎么传的怎么取
        //1. 取参
//        String paramJson = IOUtils.toString(
//                req.getInputStream(), "UTF-8");
//        HashMap<String, Object> parseObject =
//                JSON.parseObject(paramJson,
//                        HashMap.class);
//        //2.从点击界面中获取bookId的值
//        int id = (int) parseObject.get("id");
        //这边就直接通过req.getParameter直接取id参数
        int id = Integer.parseInt(req.getParameter("id"));

        //3.获取bookId的列表，然后通过book的id进行查询，查出这本书,将查出来的这本书的信息封装到了book对象中
       Book book = bookService.selectByBookId(id);

        //方式一转发,将book对象放到request中，key为book
        req.setAttribute("book", book);
        req.getRequestDispatcher("/lookBook.jsp").forward(req, resp);

        //方式二
        //将book对象放到session中，key为currentBook
        //req.getSession().setAttribute("currentBook", book);
        //重定向到详情界面
        //resp.sendRedirect("/lookBook.jsp");
    }

    }

