package cn.edu.niit.servlet;

import cn.edu.niit.javabean.Book;
import cn.edu.niit.service.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/30 0:14
 * @Version 1.0
 */
@WebServlet(name = "UpdateBook",urlPatterns = "/book/updateBook")
public class UpdateBook extends HttpServlet {
    private BookService bookService=new BookService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id=Integer.valueOf(req.getParameter("id"));
        Book book=bookService.selectByBookId(id);
        req.setAttribute("fresh", true);
        req.setAttribute("book",book);
        req.getRequestDispatcher("/admin/updateBook.jsp").forward(req, resp);

    }
}
