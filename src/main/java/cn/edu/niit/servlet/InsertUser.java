package cn.edu.niit.servlet;

import cn.edu.niit.javabean.User;
import cn.edu.niit.service.UserService;
import cn.edu.niit.util.ConString;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.UUID;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/28 22:45
 * @Version 1.0
 */
@WebServlet(name = "InsertUser",urlPatterns = "/book/insertUser")
public class InsertUser extends HttpServlet {
    private UserService userService=new UserService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接受参数

        User user=new User();
        user.setUsername(req.getParameter("username"));
        user.setEmail(req.getParameter("email"));
        user.setPassword(req.getParameter("password"));
        user.setCellphone(req.getParameter("cellphone"));
        user.setReader(req.getParameter("nickname"));
        String sex=req.getParameter("sex");
        boolean value=false;
        if ("男".equals(sex)) {
            value= true;
        } else {
            value = false;
        }
        user.setSex(value);



        userService.insertUser(user);
        resp.sendRedirect("/admin/userList.jsp");

    }
}
