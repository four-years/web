package cn.edu.niit.service;

import cn.edu.niit.dao.BookDao;
import cn.edu.niit.javabean.Book;

import java.util.List;

public class BookService {
    private BookDao bookDao=new BookDao();
    public List<Book> searchAllBooks(String username,int pageNum, int pageSize) {
        List<Book> books=bookDao.selectAll(pageNum,pageSize);
        for (Book book : books) {
            book.setRent(isRent(username,String.valueOf(book.getId())));
            book.setCollection(isCollection(username,String.valueOf(book.getId())));
        }
        return books;
    }

    public int countNum() {
        return bookDao.selectAllCount();
    }
    public boolean isRent(String username, String bookId) {
        return bookDao.selectRent(username, bookId);
    }
    public String rentBook(String username, String bookId) {
        if(!bookDao.selectRent(username, bookId)){
            int result = bookDao.insertRentBook(username, bookId);

            if (result > 0) {

                return "借阅成功";
            } else {
                return "借阅失败";
            }
        }else {
            int result= bookDao.deleteRentBook(username, bookId);
            if(result>0){
                return "取消借阅";
            }else{
                return "删除失败";
            }
        }
    }

    public boolean isCollection(String username, String bookId) {
        return bookDao.selectCollection(username, bookId);
    }
    public String collectionBook(String username, String bookId) {
        if(!bookDao.selectCollection(username, bookId)){
            int result = bookDao.insertCollectionBook(username, bookId);

            if (result > 0) {

                return "收藏成功";
            } else {
                return "收藏失败";
            }
        }else {
            int result= bookDao.deleteCollectionBook(username, bookId);
            if(result>0){
                return "取消收藏";
            }else{
                return "删除失败";
            }
        }
    }
    public Book selectByBookId(int id){
        return bookDao.selectByBookId(id);
    }
    public List<Book> selectAllRent(int pageNum, int pageSize,String username){

        return bookDao.selectAllRent(pageNum,pageSize,username);
    }
    public int insertBook(Book book){
        return bookDao.insertBook(book);
    }
    public int updateBook(Book book){
        return bookDao.updateBook(book);
    }
    public int deleteBook(int bookId){
        return bookDao.deleteBook(bookId);
    }
    public List<Book> search(String keyword) {
        return bookDao.search(keyword);
    }
}
