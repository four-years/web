package cn.edu.niit.service;

import cn.edu.niit.dao.AdminUserDao;
import cn.edu.niit.javabean.User;

import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/28 18:45
 * @Version 1.0
 */
public class UserService {

    private AdminUserDao adminUserDao=new AdminUserDao();
    public int updateUser(User user){
       return adminUserDao.updateUser(user);
    }
    public int insertUser(User user){
        return adminUserDao.insertUser(user);
    }
    public int deleteUser(int id){
        return adminUserDao.deleteUser(id);
    }
    public List<User> userList(int pageNum, int pageSize){
        return adminUserDao.userList(pageNum,pageSize);
    }
    public User selectById(int id){

        return adminUserDao.selectById(id);
    }
}
