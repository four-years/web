package cn.edu.niit.service;

import cn.edu.niit.dao.CollectionDao;
import cn.edu.niit.javabean.Collection;

import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/18 16:13
 * @Version 1.0
 */
public class CollectionService {
    private CollectionDao collectionDao=new CollectionDao();
    public List<Collection> collectionList(String username,int pageNum,int pageSize){
        return collectionDao.collectionList(pageNum,pageSize,username);
    }
    public int countNum() {
        return collectionDao.selectAllCount();
    }
}
