package cn.edu.niit.service;

import cn.edu.niit.dao.CommentDao;
import cn.edu.niit.javabean.Comment;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/7 11:17
 * @Version 1.0
 */
public class CommentService {
    private CommentDao commentDao=new CommentDao();
    public List<Comment> commentList(int id){
        return commentDao.commentList(id);
    }
    public List<Comment> findParent(List<Comment> comments){
        for(Comment comment:comments){
            ArrayList<Comment> fatherChildren=new ArrayList<>();
            //递归处理子级的回复，即回复内有回复
            findChildren(comment,fatherChildren);
            comment.setChildren(fatherChildren);
        }
        return comments;
    }
    public void findChildren(Comment parent,List<Comment> fatherChildren){
        List<Comment> comments=parent.getChildren();
        for(Comment comment:comments){
            if(!comment.getChildren().isEmpty()){
                findChildren(comment,fatherChildren);
            }
            fatherChildren.add(comment);
            comment.setChildren(new ArrayList<Comment>());
        }

    }
}
