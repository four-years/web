package cn.edu.niit.service;

import cn.edu.niit.dao.MessageBorderDao;
import cn.edu.niit.javabean.MessageBorder;

import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/26 14:59
 * @Version 1.0
 */
public class MessageBorderService {
    private MessageBorderDao messageBorderDao=new MessageBorderDao();
    public List<MessageBorder> messageBorder(){
        return messageBorderDao.messageBorderList();
    }
    public int insertMessageBorder(String name,String content){
        return messageBorderDao.insertMessage(name,content);
    }
}
