package cn.edu.niit.service;


import cn.edu.niit.dao.LoginDao;
import cn.edu.niit.javabean.Admin;
import cn.edu.niit.javabean.User;
import cn.edu.niit.javabean.dto.UserDto;

import javax.servlet.http.HttpSession;

public class LoginService {
    private LoginDao loginDao=new LoginDao();
    public String login(String username, String password, HttpSession session) {
        User user=loginDao.selectOne(username);
        if(user==null){
            return "用户不存在";
        }else{
            if(password.equals(user.getPassword())){
                //把值传递给session
                session.setAttribute("id",user.getId());
                session.setAttribute("user",user);
                session.setAttribute("isLogin",true);
                session.setAttribute("id",user.getUsername());
                return "登录成功";
            }else{
                return "密码错误";
            }

        }
    }
   public int addUser(UserDto userDto){
       return loginDao.addUser(userDto);
   }

    public String register(UserDto userDto) {
        int result = loginDao.addUser(userDto);
        if (result > 0) {
            return "注册成功";
        } else {
            return "用户已存在";
        }
    }

    public User getUserInfo(String username) {
        return loginDao.selectOne(username);
    }

    public String uploadUserInfo(User user, HttpSession session) {
        int result = 0;
        result = loginDao.updateOne(user);
        if (result > 0) {
            User userInfo = getUserInfo(user.getUsername());
            session.setAttribute("user", userInfo);
            return "更新成功";
        }
        return "更新失败";
    }
}
