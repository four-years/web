package cn.edu.niit.service;

import cn.edu.niit.dao.AdminDao;
import cn.edu.niit.javabean.Admin;

import javax.servlet.http.HttpSession;

public class AdminService {
    private AdminDao adminDao=new AdminDao();

    public String login(String username, String password, HttpSession session) {
        Admin admin=adminDao.selectOne(username);

        if(admin==null){
            return "用户不存在";
        }else{
            if(password.equals(admin.getAccount())){
                //把值传递给session
                session.setAttribute("admin",admin);
                session.setAttribute("isLogin",true);
                return "登录成功";
            }else{
                return "密码错误";
            }

        }
    }

}
