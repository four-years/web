package cn.edu.niit.javabean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


import java.util.Date;
import java.util.List;


/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/7 10:00
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Comment {
    private String header;
    private String reader;
    private Date createTime;
    private String comment;
    private List<Comment> children;


    public Comment(String header, String reader, Date createTime, String comment) {
        this.header = header;
        this.reader = reader;
        this.createTime = createTime;
        this.comment = comment;
    }
}
