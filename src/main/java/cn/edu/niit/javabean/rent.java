package cn.edu.niit.javabean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/7 9:27
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class rent {
    private int id;
    private int userId;
    private int bookId;
    private Date createTime;
    private Date updateTime;
}
