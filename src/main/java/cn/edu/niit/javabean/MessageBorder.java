package cn.edu.niit.javabean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/26 14:29
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MessageBorder {
    private int id;
    private int userId;
    private String content;
    private String createTime;
    private String header;
    private String reader;
}
