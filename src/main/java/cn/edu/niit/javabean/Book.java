package cn.edu.niit.javabean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private int id;
    private int libraryId;
    private int sortId;
    private String name;
    private String author;
    private String sort;
    private String description;
    private String position;
    private Date createTime;
    private boolean rent;
    private boolean collection;
    private boolean comment;
    private String picBook;

    public Book(int id, String name, String author, String sort,
                String description) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.sort = sort;
        this.description = description;
    }



    public Book(int id, String name, String author, String sort, String description, Date createTime) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.sort = sort;
        this.description = description;
        this.createTime = createTime;
    }

    public Book(int id, String name, String author, String description) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;

    }

    public Book(int id, String name, String author, int sortId, String description) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.sortId=sortId;
        this.description = description;
    }

    public Book(int id, String name, String author, String sort, String description, String position) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;
        this.sort=sort;
        this.position=position;

    }
}
