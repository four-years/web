package cn.edu.niit.test;

import cn.edu.niit.dao.AdminUserDao;
import cn.edu.niit.javabean.User;
import cn.edu.niit.service.LoginService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/28 17:57
 * @Version 1.0
 */
public class UserTest {
    private AdminUserDao adminUserDao=new AdminUserDao();
    @Test
    public void insertLogin(){
        User user=new User();
        user.setPassword("123");
        user.setUsername("123");
        user.setCellphone("18315158123");

        user.setEmail("1459816790@qq.com");
        adminUserDao.insertUser(user);
    }
    @Test
    public void deleteUser(){
        adminUserDao.deleteUser(1805010224);
    }
    @Test
    public void updateUser(){
        User user=new User();
        user.setPassword("121");
        user.setUsername("123");
        user.setCellphone("18315158123");
        user.setEmail("1459816790@qq.com");
        user.setId(1805010222);
        adminUserDao.updateUser(user);
    }
    @Test
    public void selectAllUser(){
        List<User> lists= adminUserDao.userList(2,2);
        for(User user:lists){
            System.out.println(user);
        }

    }
    @Test
    public void selectById(){
        User user=adminUserDao.selectById(1);
        System.out.println(user.toString());
    }
}
