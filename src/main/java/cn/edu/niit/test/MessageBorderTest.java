package cn.edu.niit.test;

import cn.edu.niit.javabean.MessageBorder;
import cn.edu.niit.service.MessageBorderService;
import org.junit.Test;

import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/26 15:01
 * @Version 1.0
 */
public class MessageBorderTest {
    private MessageBorderService messageBorderService=new MessageBorderService();
    @Test
    public void messageBorderList(){
       List<MessageBorder> lists= messageBorderService.messageBorder();
       for(MessageBorder m:lists){
           System.out.println(m);
       }
    }
    @Test
    public void insertMessage(){
        int i=messageBorderService.insertMessageBorder("123","今天天气真好！");
        if(i<0){
            System.out.println("跟新失败");
        }else{
            System.out.println("更新成功");
        }

    }
}
