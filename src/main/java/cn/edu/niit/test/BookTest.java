package cn.edu.niit.test;

import cn.edu.niit.javabean.Book;
import cn.edu.niit.service.BookService;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/28 16:24
 * @Version 1.0
 */
public class BookTest {
    private BookService bookService=new BookService();
    @Test
    public void insertBook(){
        Book book=new Book();
        book.setName("十万个为什么");
        book.setAuthor("张三");
        book.setPicBook("/header/like.jpg");
        book.setPosition("三楼");
        book.setDescription("时间刚刚好");
        book.setSortId(1);
        book.setLibraryId(1);
        int i=bookService.insertBook(book);
        if(i>0){
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }
    }
    @Test
    public void updateBook(){
        Book book=new Book();
        book.setName("十万");
        book.setAuthor("张三");
        book.setPicBook("/header/like.jpg");
        book.setPosition("三楼");
        book.setDescription("时间刚刚好");
        book.setSortId(1);
        book.setLibraryId(1);
        book.setId(24);
        int i=bookService.updateBook(book);
        if(i<1){
            System.out.println("更新失败");
        }else{
            System.out.println("更新成功"+i);
        }
    }
    @Test
    public void deleteBook(){
       int i= bookService.deleteBook(29);
        if(i<1){
            System.out.println("删除失败");
        }else{
            System.out.println("删除成功"+i);
        }
    }
    @Test
    public void selectBookById(){
        Book book=bookService.selectByBookId(2);
        System.out.println(book.toString());

    }
    @Test
    public void search(){
        List<Book> book=bookService.search("图");
        for(Book book1:book){
            System.out.println(book1);
        }

    }
}
