package cn.edu.niit.test;

import cn.edu.niit.dao.CommentDao;
import cn.edu.niit.javabean.Comment;
import org.junit.Test;


import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/10 15:07
 * @Version 1.0
 */
public class CommentTest {
    private CommentDao commentDao=new CommentDao();

    @Test
    public void commentList(){
       List<Comment> comments=commentDao.commentList(1);
        for(Comment comment:comments){
            System.out.println(comment);
        }
    }
}
