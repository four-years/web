package cn.edu.niit.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "AdminFilter", urlPatterns = "/login")
public class AdminFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String role= request.getParameter("role");
        if("0".equals(role)){
            request.getRequestDispatcher("/admin_login").forward(request,response);
        }else{
            request.getRequestDispatcher("/login").forward(request,response);

        }
    }

    @Override
    public void destroy() {

    }
}
