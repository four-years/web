package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.Comment;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/7 10:03
 * @Version 1.0
 */
public class CommentDao {
    public List<Comment> commentList(int bookId){
        String sql="select borrow_card.header,borrow_card.reader,comment.book_id,comment.create_time,comment.comment from comment,borrow_card " +
                "where borrow_card.username=comment.user_id and book_id=?";
        List<Comment> comments=new ArrayList<>();
        try(ResultSet rs= JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{bookId})){
            while(rs.next()){
                Comment comment=new Comment(rs.getString("header"),rs.getString("reader"),rs.getDate("create_time"),rs.getString("comment"));
                comments.add(comment);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return comments;
    }
    public List<Comment> findParent(List<Comment> comments){
        String sql="select borrow_card.header,borrow_card.reader,comment.book_id,comment.create_time,comment.comment from comment,borrow_card " +
                "where borrow_card.username=comment.user_id and book_id=? and parent_id=?";
        List<Comment> comments1=new ArrayList<>();
        try(ResultSet rs=JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{
        })){
            while(rs.next()){
                Comment comment=new Comment(rs.getString("header"),rs.getString("reader"),rs.getDate("create_time"),rs.getString("comment"));
                comments.add(comment);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return comments;
    }
}
