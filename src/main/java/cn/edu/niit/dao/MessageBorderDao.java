package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.MessageBorder;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/26 14:31
 * @Version 1.0
 */
public class MessageBorderDao {
    public List<MessageBorder> messageBorderList(){
        List<MessageBorder> lists=new ArrayList<>();
        String sql="select message_bord.*,borrow_card.header,borrow_card.reader from message_bord,borrow_card where user_id=borrow_card.username ORDER BY create_time DESC";
        try(ResultSet rs= JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{

        })){
            while(rs.next()){
                MessageBorder messageBorder=new MessageBorder(rs.getInt("id"),rs.getInt("user_id"),
                        rs.getString("content"),rs.getString("create_time"),rs.getString("header"),rs.getString("reader"));
                lists.add(messageBorder);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return lists;
    }
    public int insertMessage(String username,String content){
        String sql="INSERT into message_bord(user_id,content,create_time) VALUES(?,?,?)";
        return JDBCUtil.getInstance().executeUpdate(sql,
                new Object[]{
                        username,content,
                        new Date(System.currentTimeMillis())
                });
    }
}
