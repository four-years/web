package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.User;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/28 17:17
 * @Version 1.0
 */
public class AdminUserDao {
    public int insertUser(User user){
        String sql="insert into borrow_card(id,username,password,reader,sex,cellphone,email) values(?,?,?,?,?,?,?)";
        return JDBCUtil.getInstance().executeUpdate(sql,new Object[]{
                null,user.getUsername(),user.getPassword(),user.getReader(),user.isSex(),user.getCellphone(),user.getEmail()
        });
    }
    public int deleteUser(int userId){
        String sql="delete from borrow_card where id=?";
        return JDBCUtil.getInstance().executeUpdate(sql,new Object[]{
            userId
        });
    }
    public int updateUser(User user){
        String sql="update borrow_card set password=?,reader=?,username=?,cellphone=?,email=?,sex=? where id=?";
        return JDBCUtil.getInstance().executeUpdate(sql,new Object[]{
                user.getPassword(),user.getReader(),user.getUsername(),user.getCellphone(),user.getEmail(),user.isSex(),user.getId()
        });
    }
    public User selectById(int id){
        User user=null;
        String sql="select * from borrow_card where id=?";
        try (ResultSet resultSet=JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{
                id
        })){
            while (resultSet.next()){
                user=new User(
                        resultSet.getInt("id"),
                        resultSet.getString("username"),
                        resultSet.getString("password"),
                        resultSet.getString("reader"),
                        resultSet.getString("header"),
                        resultSet.getString("cellphone"),
                        resultSet.getString("email"),
                        resultSet.getString("describe"),
                        resultSet.getBoolean("sex"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }
    public List<User> userList(int pageNum, int pageSize){
        List<User> lists=new ArrayList<>();
        String sql="select * from borrow_card limit ?,?";
        try(ResultSet resultSet=JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{
                (pageNum - 1) * pageSize, pageSize
        })) {
            while (resultSet.next()){
                User user=new User(
                        resultSet.getInt("id"),
                        resultSet.getString("username"),
                        resultSet.getString("password"),
                        resultSet.getString("reader"),
                        resultSet.getString("header"),
                        resultSet.getString("cellphone"),
                        resultSet.getString("email"),
                        resultSet.getString("describe"),
                        resultSet.getBoolean("sex"));
                lists.add(user);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return lists;
    }
}
