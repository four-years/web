package cn.edu.niit.dao;


import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.User;
import cn.edu.niit.javabean.dto.UserDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class LoginDao {
//登录
    public User selectOne(String username) {
        User user = null;
        try (ResultSet resultSet =
                     JDBCUtil.getInstance().executeQueryRS("select " +
                                     "* " +
                                     "from " +
                                     "borrow_card where username=?",
                             new Object[]{username})) {

            while (resultSet.next()) {
                user = new User(resultSet.getString("username"),
                        resultSet.getString("password"),
                        resultSet.getString("reader"),
                        resultSet.getString("header"),
                        resultSet.getString("cellphone"),
                        resultSet.getString("email"),
                        resultSet.getString("describe"),
                        resultSet.getBoolean("sex")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
    //添加用户
    public int addUser(UserDto userDto){
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        try{
            con=JDBCUtil.getInstance().getConnection();
            ps=con.prepareStatement("insert into borrow_card(username, password) values (?,?)");
            ps.setString(1,userDto.getAccount());
            ps.setString(2,userDto.getPassword());
            return ps.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
           try{
               JDBCUtil.close(con,ps,null);
           }catch (Exception e){
               e.printStackTrace();
           }
        }

        return -1;

    }
    //更新用户的个人信息
    public int updateOne(User user) {
        int result = 0;
        StringBuilder sb = new StringBuilder("update borrow_card " +
                "set reader=?, cellphone=?, email=?, sex=?, " +
                "borrow_card.`describe`=? ");
        if (user.getHeader() != null) {
            sb.append(", header=? where " +
                    "username=?");
            result =
                    JDBCUtil.getInstance().executeUpdate(sb.toString(),
                            new Object[]{user.getReader(),
                                    user.getCellphone(),
                                    user.getEmail(), user.isSex(),
                                    user.getDescribe(),
                                    user.getHeader(),
                                    user.getUsername()});
        } else {
            sb.append("where username=?");
            result =
                    JDBCUtil.getInstance().executeUpdate(sb.toString(),
                            new Object[]{user.getReader(),
                                    user.getCellphone(),
                                    user.getEmail(), user.isSex(),
                                    user.getDescribe(),
                                    user.getUsername()});
        }
        return result;
    }
    }

