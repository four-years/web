package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.Collection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangli
 * @Description TODO
 * @Date 2021/5/18 15:40
 * @Version 1.0
 */
public class CollectionDao {
    public List<Collection> collectionList(int pageNum, int pageSize,String username){
        List<Collection> collectionList=new ArrayList<>();
        String sql="select books.pic_book,books.author,books.name,books.description,collection.create_time from books,collection where collection.user_id=? and books.id=collection.book_id limit ?,?";
        try(ResultSet rs= JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{username,(pageNum - 1) * pageSize, pageSize})){
            while(rs.next()){
                Collection collection=new Collection(rs.getString("pic_book"),rs.getString("author"),rs.getString("name"),
                        rs.getString("description"),rs.getDate("create_time"));
                collectionList.add(collection);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return collectionList;
    }
    public int selectAllCount() {
        String sql = "select count(*) as num from collection";
        try (final ResultSet rs =
                     JDBCUtil.getInstance().executeQueryRS(sql,
                             new Object[]{})) {
            while (rs.next()) {
                return rs.getInt("num");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

}
