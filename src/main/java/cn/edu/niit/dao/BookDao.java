package cn.edu.niit.dao;

import cn.edu.niit.db.JDBCUtil;
import cn.edu.niit.javabean.Book;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookDao {
    long l=System.currentTimeMillis();//得到long类型当前时间
    Date date=new Date(l);
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public List<Book> selectAll(int pageNum, int pageSize) {
        String sql = "select books.*, book_sort.name as sort from " +
                "books, " +
                "book_sort where books.sort_id=book_sort.id limit " +
                "?,?";
        List<Book> books = new ArrayList<>();
        try (ResultSet rs = JDBCUtil.getInstance() .executeQueryRS(sql,
                new Object[]{(pageNum - 1) * pageSize, pageSize})) {
            while (rs.next()) {
                Book book = new Book(rs.getInt("id"),rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("sort"),
                        rs.getString("description"));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }


    public int selectAllCount() {
        String sql = "select count(*) as num from books";
        try (final ResultSet rs =
                     JDBCUtil.getInstance().executeQueryRS(sql,
                             new Object[]{})) {
            while (rs.next()) {
                return rs.getInt("num");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public boolean selectRent(String username, String bookId) {
        String sql1 = "select EXISTS( SELECT 1 from rent " +
                "where book_id=? and user_id=?) as rent";
        try (ResultSet rs =
                     JDBCUtil.getInstance().executeQueryRS(sql1,
                             new Object[]{
                                     bookId, username
                             });) {

            while (rs.next()) {
                return rs.getBoolean("rent");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
    public int deleteRentBook(String username,String bookId){
        String sql="delete from rent where book_id=? and user_id=?";
        int result=JDBCUtil.getInstance().executeUpdate(sql,new Object[]{
                bookId,username
        });
        return result;
    }
    public int insertRentBook(String username, String bookId) {
        String sql = "insert into rent(book_id, user_id, " +
                "create_time, update_time) values(?,?,?,?)";
        int result = JDBCUtil.getInstance().executeUpdate(sql,
                new Object[]{
                        bookId, username,
                        dateFormat.format(date),
                        dateFormat.format(date)
                });
        return result;
    }
    public boolean selectCollection(String username, String bookId) {
        String sql1 = "select EXISTS( SELECT 1 from collection " +
                "where book_id=? and user_id=?) as collection";
        try (ResultSet rs =
                     JDBCUtil.getInstance().executeQueryRS(sql1,
                             new Object[]{
                                     bookId, username
                             });) {

            while (rs.next()) {
                return rs.getBoolean("collection");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
    public int deleteCollectionBook(String username,String bookId){
        String sql="delete from collection where book_id=? and user_id=?";
        int result=JDBCUtil.getInstance().executeUpdate(sql,new Object[]{
                bookId,username
        });
        return result;
    }
    public int insertCollectionBook(String username, String bookId) {
        String sql = "insert into collection(book_id, user_id, " +
                "create_time, update_time) values(?,?,?,?)";
        int result = JDBCUtil.getInstance().executeUpdate(sql,
                new Object[]{
                        bookId, username,
                        dateFormat.format(date),
                        dateFormat.format(date)
                });
        return result;
    }
    //根据图书编号查找图书的信息
    public Book selectByBookId(int id){
        String sql = "select books.*, book_sort.name as sort from books, book_sort where books.sort_id=book_sort.id and books.id=?";
        Book book=new Book();
            try (ResultSet rs=JDBCUtil.getInstance().executeQueryRS(sql,new Object[]{id})){
                while (rs.next()){
                    book = new Book(rs.getInt("id"),rs.getString("name"),
                            rs.getString("author"),
                            //没有这列啊
                            rs.getString("sort"),
                            rs.getString("description"),rs.getTimestamp("create_time"));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        return book;
    }
    //借阅查询
    public List<Book> selectAllRent(int pageNum, int pageSize,String username) {
        String sql = "select books.*, rent.* from books,rent where books.id=rent.book_id and user_id=? limit ?,?";
        List<Book> books = new ArrayList<>();
        try (ResultSet rs = JDBCUtil.getInstance() .executeQueryRS(sql,
                new Object[]{username,(pageNum - 1) * pageSize, pageSize})) {
            while (rs.next()) {
                Book book = new Book(rs.getInt("id"),rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("description"));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }
    public int insertBook(Book book){
        String sql="insert into books(name,author,sort_id,description) VALUES (?,?,?,?)";
        int result = JDBCUtil.getInstance().executeUpdate(sql,
                new Object[]{
                        book.getName(),book.getAuthor(),book.getSortId(),book.getDescription()

                });
        return result;
    }
    public int deleteBook(int id){
        String sql="delete from books where id=?";
        int result=JDBCUtil.getInstance().executeUpdate(sql,new Object[]{
                    id
                });
        return result;

    }
    public int updateBook(Book book){
        String sql="UPDATE books SET name=?, author=?,description=?,create_time=?,pic_book=?,position=?,library_id=?,sort_id=? where id=?";
        int result = JDBCUtil.getInstance().executeUpdate(sql,
                new Object[]{
                       book.getName(),book.getAuthor(),book.getDescription(),dateFormat.format(date),book.getPicBook(),book.getPosition(),book.getLibraryId(),book.getSortId(),book.getId()

                });
        return result;
    }
    public List<Book> search(String keyword) {


        String sql = "select books.*,book_sort.name as sort from books,book_sort where books.sort_id=book_sort.id and books.name like ?";
        List<Book> books = new ArrayList<>();
        try (ResultSet rs = JDBCUtil.getInstance().executeQueryRS(sql,
                new Object[]{"%"+keyword+"%"})) {
            while (rs.next()) {
                Book book = new Book(rs.getInt("id"),rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("sort"),
                        rs.getString("description"),
                        rs.getString("position"));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;


    }



}

